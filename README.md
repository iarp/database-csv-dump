Database CSV Dump
=================

See wiki for configuration information

Downloads
---------

The Downloads page contains pre-compiled zips for Windows operating systems.

Download the latest version, rename the config-dist.ini to config.ini and update the sections you care for accordingly.

Building From Source Requirements
---------------------------------

- Python 3.4
- pyodbc for SQL Server - https://code.google.com/p/pyodbc/
- mysql.connector for MySQL - http://dev.mysql.com/downloads/connector/python/

Make whatever changes you wish to the source code and run "python.exe setup.py build" to build your own zip file.

MSVCR100.dll is missing
-----------------------

If you get an error message about MSVCR100.dll missing you need to install the software linked below according to your
Windows bitness.

- 32 bit: http://www.microsoft.com/download/en/details.aspx?id=5555
- 64 bit: http://www.microsoft.com/download/en/details.aspx?id=14632

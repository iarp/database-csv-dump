import sys
import os
from PySide.QtCore import *
from PySide.QtGui import *
from gui_codes import main_gui, popup_gui
import functions
import traceback
import dump_data_to_csv
import connectors
from collections import OrderedDict


class PopupGUI(QDialog, popup_gui.Ui_Dialog):

    value = 1
    last_button_clicked = None

    def __init__(self, parent=None, value=1):
        super(PopupGUI, self).__init__(parent)
        self.setupUi(self)
        self.value = value

        self.txt_splits.setText(str(self.value))

    @Slot()
    def on_btn_ok_clicked(self):
        self.last_button_clicked = 'ok'
        self.value = int(self.txt_splits.text())
        self.hide()

    @Slot()
    def on_btn_cancel_clicked(self):
        self.last_button_clicked = 'cancel'


class MainGUI(QDialog, main_gui.Ui_Dialog):

    available_databases_items = []
    selected_databases_items = []

    database_tables = {}
    mapped_tables = {}
    split_tables = {}

    config = {}

    connector = None

    def __init__(self, parent=None):
        super(MainGUI, self).__init__(parent)
        self.setupUi(self)

        self.list_available_databases.itemDoubleClicked.connect(self.add_selected_database)
        # self.list_selected_databases.itemDoubleClicked.connect(self.remove_selected_database)
        self.list_selected_databases.itemClicked.connect(self.display_selected_database_tables)
        self.list_selected_database_tables.itemDoubleClicked.connect(self.add_database_table_to_selection)
        self.list_mapped_tables.itemDoubleClicked.connect(self.remove_database_table_from_selection)

        self.add_right_click_menu_items()

        self.rdo_mysql.toggled.connect(self.click_radio_change_port)

        self.config = functions.LoadConfig('config.ini')

        self.txt_sql_hostname.setText(self.config.get('server', {}).get('hostname', '127.0.0.1'))
        self.txt_sql_port.setValue(int(self.config.get('server', {}).get('port', 1433)))
        self.txt_sql_username.setText(self.config.get('server', {}).get('username', 'sa'))
        self.txt_sql_password.setText(str(self.config.get('server', {}).get('password', '')))

        if self.config.get('server', {}).get('version', 'MSSQL').upper() == 'MSSQL':
            self.rdo_mssql.setChecked(True)
        else:
            self.rdo_mysql.setChecked(True)

        self.txt_encoding_read.setText(self.config.get('settings', {}).get('encoding_read', 'latin1'))
        self.txt_encoding_write.setText(self.config.get('settings', {}).get('encoding_write', 'utf-8'))
        self.txt_save_location.setText(self.config.get('settings', {}).get('export_folder', 'exports/'))
        self.chk_delete_blanks.setChecked(self.config.get('settings', {}).get('delete_empty_files', False))
        self.txt_echo_position.setProperty("value", int(self.config.get('settings', {}).get('echo_position', 10000)))
        self.txt_csv_delimiter.setText(self.config.get('settings', {}).get('csv_delimiter', ','))
        self.txt_csv_quotation.setText(self.config.get('settings', {}).get('csv_quotation', '"'))

        mssql_installed = True
        mysql_installed = True

        try:
            import pyodbc
        except ImportError:
            self.rdo_mssql.setEnabled(False)
            mssql_installed = False

        try:
            import mysql.connector
        except ImportError:
            self.rdo_mysql.setEnabled(False)
            mysql_installed = False

        # Disable dump data and sql connect buttons if mysql and mssql drivers are not installed.
        if not mysql_installed and not mssql_installed:
            self.btn_dump_data.setEnabled(False)
            self.btn_sql_connect.setEnabled(False)

            self.simple_popup('Both MySQL and SQL Server drivers are not installed. Ensure mysql.connector or pyodbc are installed!')

    def click_radio_change_port(self):
        """ When the SQL radio buttons change, update the port number to the default values.

        :return:
        """
        if self.rdo_mysql.isChecked():
            self.txt_sql_port.setValue(3306)
        elif self.rdo_mssql.isChecked():
            self.txt_sql_port.setValue(1433)

    def display_selected_database_tables(self):
        """ When a database is selected, we need to update the 2 right lists with
            the selected databases tables

        :return:
        """
        selected_database = self.list_selected_databases.selectedItems()[0].text()

        self.update_lists_tables(selected_database)

    def remove_selected_database(self):
        """ Removes a database from selection along with any tables that may have been mapped.

        :return:
        """
        for database in self.list_selected_databases.selectedItems():
            selected_database = database.text()
            self.available_databases_items.append(selected_database)
            self.selected_databases_items.remove(selected_database)

            self.mapped_tables[selected_database].clear()
            self.split_tables[selected_database].clear()

            self.list_selected_database_tables.clear()
            self.list_mapped_tables.clear()

            self.update_lists()

    def add_selected_database(self):
        """ Adds a new table to the selection

        :return:
        """
        for database in self.list_available_databases.selectedItems():
            database = database.text()
            self.available_databases_items.remove(database)
            self.selected_databases_items.append(database)
            self.update_lists()

    @Slot()
    def on_btn_sql_connect_clicked(self):

        hostname = self.txt_sql_hostname.text()
        username = self.txt_sql_username.text()
        password = self.txt_sql_password.text()

        self.reset_vars()

        if not hostname or not username:
            self.simple_popup('Hostname and Username details are required.', 'Missing SQL Connection details')
            return

        if self.rdo_mysql.isChecked():
            self.connector = connectors.MySQL()

        elif self.rdo_mssql.isChecked():
            self.connector = connectors.SQLServer()

        else:
            print('System Error, somehow no valid radio is selected')
            return False

        success, error_message = self.connector.connect(hostname, username, password)

        if not success:

            self.btn_sql_connect.setText(error_message)

        else:

            self.btn_sql_connect.setText('SQL Connected')

            self.get_database_tables()

    def get_database_tables(self):
        """ Obtain this connectors all available user databases.

        :return:
        """
        self.available_databases_items.clear()

        for database_name in self.connector.get_user_databases():

            self.available_databases_items.append(database_name)

            if database_name not in self.database_tables:
                self.database_tables[database_name] = set()
                self.mapped_tables[database_name] = set()
                self.split_tables[database_name] = {}

            self.database_tables[database_name] = self.connector.get_database_tables(database_name)

        self.update_lists()

    def reset_vars(self):
        """ Used when changing servers to ensure we don't save anything from the previous server.

        :return:
        """
        self.mapped_tables = {}
        self.split_tables = {}
        self.database_tables = {}
        self.available_databases_items = []
        self.selected_databases_items = []

        self.list_selected_database_tables.clear()
        self.list_mapped_tables.clear()

        self.update_lists()

    def update_lists(self):
        """ Updates the two left most database lists

        :return:
        """
        self.list_available_databases.clear()
        self.list_selected_databases.clear()

        for item in sorted(self.available_databases_items):
            self.list_available_databases.addItem(item)

        for item in sorted(self.selected_databases_items):
            self.list_selected_databases.addItem(item)

    def update_lists_tables(self, database):
        """ Updates the two right most table lists for the selected database

        :param database:
        :return:
        """
        self.list_selected_database_tables.clear()
        self.list_mapped_tables.clear()

        # Add all tables that are not already mapped to the available tables list
        for table in sorted(self.database_tables[database]):
            if table not in self.mapped_tables[database]:
                self.list_selected_database_tables.addItem(table)

        # Add the mapped tables to the mapped list
        for table in sorted(self.mapped_tables[database]):
            self.list_mapped_tables.addItem(table)

    def add_database_table_to_selection(self, item=None, splits_value=None):
        """ Adds a table to be dumped to the mapped system.

        :return:
        """
        if not self.list_selected_database_tables.selectedItems():
            return

        # Prepare the Splits popup
        splits_popup = PopupGUI(self, 1)

        # If the supplied value is None it means use the popup to obtain a value
        if splits_value is None:
            splits_popup.exec_()

            if splits_popup.last_button_clicked == 'cancel':
                return

        elif isinstance(splits_value, int):
            # Otherwise ensure it's an int value and override the default.
            splits_popup.value = splits_value

        # This should only ever loop once since you can only select one database at any one time.
        for database in self.list_selected_databases.selectedItems():
            selected_database = database.text()

            for selected in self.list_selected_database_tables.selectedItems():
                table_name = selected.text()

                self.mapped_tables[selected_database].add(table_name)
                self.split_tables[selected_database][table_name] = splits_popup.value

            self.update_lists_tables(selected_database)

    def remove_database_table_from_selection(self):
        """ Removes a table from the mapped list and re-adds it to the available tables list

        :return:
        """
        for database in self.list_selected_databases.selectedItems():
            selected_database = database.text()

            for selected in self.list_mapped_tables.selectedItems():
                self.mapped_tables[selected_database].remove(selected.text())
                del(self.split_tables[selected_database][selected.text()])

            self.update_lists_tables(selected_database)

    @Slot()
    def on_btn_dump_data_clicked(self):
        """ Save config and then dump the data

        :return:
        """
        self.on_btn_save_config_clicked()

        dump_data_to_csv.DumpDatabaseCSV(run_immediately=True)

    @Slot()
    def on_btn_save_config_clicked(self):
        """ Saves the current configuration to the config.ini file for processing

        :return:
        """
        config = OrderedDict()

        if self.rdo_mssql.isChecked():
            sql_version = 'MSSQL'
        elif self.rdo_mysql.isChecked():
            sql_version = 'MYSQL'
        else:
            print('Failed to save, invalid radio select')
            return None

        config['server'] = OrderedDict([
            ('hostname', self.txt_sql_hostname.text()),
            ('port', self.txt_sql_port.text()),
            ('username', self.txt_sql_username.text()),
            ('password', self.txt_sql_password.text()),
            ('version', sql_version)
        ])

        config['settings'] = OrderedDict([
            ('encoding_write', self.txt_encoding_write.text()),
            ('encoding_read', self.txt_encoding_read.text()),
            ('export_folder', self.txt_save_location.text()),
            ('delete_empty_files', self.chk_delete_blanks.isChecked()),
            ('echo_position', self.txt_echo_position.text()),
            ('csv_delimiter', self.txt_csv_delimiter.text()),
            ('csv_quotation', self.txt_csv_quotation.text()),
        ])

        for database, tables in self.mapped_tables.items():
            config_splits_name = 'Splits:{}'.format(database)

            config_db_name = 'Database:{}'.format(database)
            if tables and config_db_name not in config:
                config[config_db_name] = {}

            for table in tables:
                config[config_db_name][table] = 'SELECT * FROM {}'.format(table)

                if self.split_tables[database][table] > 1:

                    if tables and config_splits_name not in config:
                        config[config_splits_name] = {}

                    config[config_splits_name][table] = self.split_tables[database][table]

        functions.SaveConfig('config.ini', config)

    @staticmethod
    def simple_popup(message, title='Alert', yesno=None):
        """ Causes a simple QMessageBox to appear alerting the user to something

        :param message: string containing your message
        :param title: string containing popup title
        :param yesno: boolean whether or not you want Yes/No buttons plus a return value
        :return: If yesno is True, returns True/False depending on users selection, otherwise void
        """
        message_box = QMessageBox()
        message_box.setWindowTitle(title)
        message_box.setText(message)
        message_box.setStandardButtons(QMessageBox.Ok)
        message_box.setDefaultButton(QMessageBox.Ok)

        # By default we will only show an OK button.
        # If the yesno variable is True, change that to Yes and No
        #   buttons PLUS return the value the user selected as boolean.
        if yesno:
            message_box.setStandardButtons(QMessageBox.Yes | QMessageBox.No)
            message_box.setDefaultButton(QMessageBox.No)

            if message_box.exec_() == QMessageBox.Yes:
                return True

            return False

        message_box.exec_()

    def add_right_click_menu_items(self):
        """ All of the right click menu options are here, corresponding functions are below this one.

        :return:
        """
        self.list_available_databases.setContextMenuPolicy(Qt.CustomContextMenu)
        self.list_available_databases.connect(self.list_available_databases,
                                              SIGNAL("customContextMenuRequested(QPoint)"),
                                              self.list_available_databases_right_click_menu)

        self.list_selected_databases.setContextMenuPolicy(Qt.CustomContextMenu)
        self.list_selected_databases.connect(self.list_selected_databases,
                                             SIGNAL("customContextMenuRequested(QPoint)"),
                                             self.list_selected_databases_right_click_menu)

        self.list_selected_database_tables.setContextMenuPolicy(Qt.CustomContextMenu)

        self.list_selected_database_tables.connect(self.list_selected_database_tables,
                                                   SIGNAL("customContextMenuRequested(QPoint)"),
                                                   self.list_selected_database_tables_right_click_menu)

        self.list_mapped_tables.setContextMenuPolicy(Qt.CustomContextMenu)

        self.list_mapped_tables.connect(self.list_mapped_tables,
                                        SIGNAL("customContextMenuRequested(QPoint)"),
                                        self.list_mapped_tables_right_click_menu)

    def list_available_databases_right_click_menu(self, position):
        self.list_menu = QMenu()
        menu_item = self.list_menu.addAction("Add Database")
        # menu_item2 = self.list_menu.addAction("Remove Database")
        self.connect(menu_item, SIGNAL("triggered()"), self.add_selected_database)
        # self.connect(menu_item2, SIGNAL("triggered()"), self.remove_selected_database)
        parent_position = self.list_available_databases.mapToGlobal(QPoint(0, 0))
        self.list_menu.move(parent_position + position)
        self.list_menu.show()

    def list_selected_databases_right_click_menu(self, position):
        self.list_menu = QMenu()
        # menu_item = self.list_menu.addAction("Add Database")
        menu_item2 = self.list_menu.addAction("Remove Database")
        # self.connect(menu_item, SIGNAL("triggered()"), self.add_selected_database)
        self.connect(menu_item2, SIGNAL("triggered()"), self.remove_selected_database)
        parent_position = self.list_selected_databases.mapToGlobal(QPoint(0, 0))
        self.list_menu.move(parent_position + position)
        self.list_menu.show()

    def list_selected_database_tables_right_click_menu(self, position):
        self.list_menu = QMenu()
        menu_item = self.list_menu.addAction("Add Table(s)")
        menu_item2 = self.list_menu.addAction("Add Table(s) (split=1)")
        menu_item3 = self.list_menu.addAction("Add Table(s) (split=5)")
        menu_item4 = self.list_menu.addAction("Add Table(s) (split=10)")
        self.connect(menu_item, SIGNAL("triggered()"), self.add_database_table_to_selection)
        self.connect(menu_item2, SIGNAL("triggered()"), self.add_database_table_to_selection_split_one)
        self.connect(menu_item3, SIGNAL("triggered()"), self.add_database_table_to_selection_split_five)
        self.connect(menu_item4, SIGNAL("triggered()"), self.add_database_table_to_selection_split_ten)
        parent_position = self.list_selected_database_tables.mapToGlobal(QPoint(0, 0))
        self.list_menu.move(parent_position + position)
        self.list_menu.show()

    def list_mapped_tables_right_click_menu(self, position):
        self.list_menu = QMenu()
        menu_item = self.list_menu.addAction("Remove Table(s)")
        self.connect(menu_item, SIGNAL("triggered()"), self.remove_database_table_from_selection)
        parent_position = self.list_mapped_tables.mapToGlobal(QPoint(0, 0))
        self.list_menu.move(parent_position + position)
        self.list_menu.show()

    def add_database_table_to_selection_split_one(self):
        """ Used by right click menu item to by-pass the splits popup and split 1 time."""
        self.add_database_table_to_selection(splits_value=1)

    def add_database_table_to_selection_split_five(self):
        """ Used by right click menu item to by-pass the splits popup and split 5 times."""
        self.add_database_table_to_selection(splits_value=5)

    def add_database_table_to_selection_split_ten(self):
        """ Used by right click menu item to by-pass the splits popup and split 10 times."""
        self.add_database_table_to_selection(splits_value=10)

if __name__ == "__main__":
    app = QApplication(sys.argv)
    form = MainGUI()
    form.show()
    app.exec_()

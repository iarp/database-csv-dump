from cx_Freeze import setup, Executable
import sys

include_files = ['config-dist.ini', 'README.md']
includes = ['easygui']
excludes = []
packages = ['decimal']

setup(
    name='Database CSV Export',
    version='3.0.0',
    description='Dumps MySQL and SQL Server database to CSV format.',
    author='IARP',
    author_email='',
    options={'build_exe': {
        'excludes': excludes,
        'packages': packages,
        'include_files': include_files
    }},
    executables=[
        Executable('dump_from_gui.py'),
        Executable('dump_data_to_csv.py'),
    ]
)

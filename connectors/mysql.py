import traceback
import sys
import os
import warnings

try:
    import mysql.connector
except ImportError:
    warnings.warn('mysql.connector ImportError, MySQL is disabled.')


class MySQL:

    connection = None
    cursor = None

    port = 3306

    version = 'MySQL'

    def connect(self, hostname, username, password, port=None):
        """ Creates the connection to the database

        Returns [success, error_message]

        :param hostname:
        :param username:
        :param password:
        :param port:
        :return:
        :rtype: list
        """
        if not port:
            port = self.port
        elif not isinstance(port, int):
            port = int(port)

        try:
            self.connection = mysql.connector.connect(host=hostname, port=port, user=username,
                                                      password=password, buffered=True)
            self.cursor = self.connection.cursor()

            return True, True
        except (mysql.connector.errors.InterfaceError, mysql.connector.errors.ProgrammingError) as e:
            code = e.args[0]
            print(traceback.format_exc())

            if code == 2003:
                return False, 'MySQL: Server not found'
            elif code == 1045:
                return False, 'MySQL: Bad Login'

        return False, 'SQL: General Failure'

    def get_user_databases(self):
        """ Obtains all user-created databases in the system.

        Excludes mysql, sys, information_schema, performance_schema

        :return:
        """
        user_dbs = set()
        for db in self.get_all_databases():
            if db not in ['information_schema', 'mysql', 'sys', 'performance_schema']:
                user_dbs.add(db)

        return user_dbs

    def get_all_databases(self):
        """ Obtains all databases in the system

        :return:
        """
        databases = set()

        self.cursor.execute("SHOW DATABASES")
        for database in self.cursor.fetchall():
            databases.add(database[0])

        return databases

    def get_database_tables(self, database):
        """ Obtain all base tables on a specified database.

        :param database:
        :return:
        """
        tables = set()

        self.select_database(database)

        self.cursor.execute('SHOW TABLES')
        for table in self.cursor.fetchall():
            tables.add(table[0])

        return tables

    def select_database(self, database):
        """ Selects the specified database

        :param database:
        :return:
        """
        self.cursor.execute('USE {}'.format(database))

    @staticmethod
    def format_row_data(columns, row, read_encoding):
        """ Formats data according to the columns

        :param columns:
        :param row:
        :param read_encoding:
        :return:
        """
        return row

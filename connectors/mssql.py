import traceback
import sys
import os
import warnings

try:
    import pyodbc
except ImportError:
    warnings.warn('pyodbc ImportError, MSSQL disabled.')


class SQLServer:

    connection = None
    cursor = None

    port = 1433

    version = 'MSSQL'

    def connect(self, hostname, username, password, port=None):
        """ Creates the connection to the database

        Returns [success, error_message]

        :param hostname:
        :param username:
        :param password:
        :param port:
        :return:
        :rtype: list
        """
        if not port:
            port = self.port
        elif not isinstance(port, int):
            port = int(port)

        try:
            self.connection = pyodbc.connect(
                driver='{SQL Server}',
                server=hostname,
                uid=username,
                pwd=password,
                port=port
            )
            self.cursor = self.connection.cursor()

            return True, True
        except pyodbc.Error as e:
            code = e.args[0]
            print(traceback.format_exc())

            if code == '28000':
                return False, 'SQL: Login Failure'
            elif code == '42000':
                return False, 'SQL: Cannot open database'
            elif code == '08001':
                return False, 'SQL: Cannot connect to hostname'

            return False, 'SQL: General Failure'

    def get_user_databases(self):
        """ Obtains all user-created databases in the system.

        Excludes master, msdb, model, and tempdb

        :return:
        """
        user_dbs = set()
        for db in self.get_all_databases():
            if db not in ['master', 'msdb', 'model', 'tempdb']:
                user_dbs.add(db)

        return user_dbs

    def get_all_databases(self):
        """ Obtains all databases in the system

        :return:
        """
        databases = set()

        self.cursor.execute("SELECT name FROM sys.databases")
        for database in self.cursor.fetchall():
            databases.add(database.name)

        return databases

    def get_database_tables(self, database):
        """ Obtain all base tables on a specified database.

        :param database:
        :return:
        """
        tables = set()

        self.cursor.execute("""SELECT TABLE_NAME FROM {}.INFORMATION_SCHEMA.TABLES
                                WHERE TABLE_TYPE = 'BASE TABLE' ORDER BY TABLE_NAME""".format(database))
        for table in self.cursor.fetchall():
            tables.add(table.TABLE_NAME)

        return tables

    def select_database(self, database):
        self.cursor.execute('USE {}'.format(database))

    @staticmethod
    def format_row_data(columns, row, read_encoding):
        """ Formats data according to the columns

        :param columns:
        :param row:
        :param read_encoding:
        :return:
        """
        row_data = []

        for column in columns:
            tmp = getattr(row, column)

            if isinstance(tmp, bytearray):
                tmp = tmp.decode(read_encoding)

            row_data.append(tmp)

        return row_data

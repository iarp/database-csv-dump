import functions
import sys
import os
import csv
import connectors
import traceback


class DumpDatabaseCSV:

    config = {}

    db_hostname = None
    db_username = None
    db_password = None
    databases = []

    database_folder = None
    database_tables = {}

    encoding_read = 'utf-8'
    encoding_write = 'utf-8'
    echo_position = 10000
    delete_empty_files = False
    export_folder = 'exports/'

    table_splits = {}

    files = {}

    empty_files = []

    csv_delimiter = ','
    csv_quotation = '"'

    def __init__(self, run_immediately=True):

        self.reset_vars()

        self.config = functions.LoadConfig('config.ini')

        hostname = self.config.get('server', {}).get('hostname', None)
        port = self.config.get('server', {}).get('port', None)
        username = self.config.get('server', {}).get('username', None)
        password = str(self.config.get('server', {}).get('password', None))

        for section, data in self.config.items():
            if section.startswith('Database:'):
                database_name = section.replace('Database:', '')
                self.databases.append(database_name)

        self.encoding_read = self.config.get('settings', {}).get('encoding_read', 'utf-8')
        self.encoding_write = self.config.get('settings', {}).get('encoding_write', 'utf-8')
        self.export_folder = self.config.get('settings', {}).get('export_folder', 'exports/')
        self.csv_delimiter = self.config.get('settings', {}).get('csv_delimiter', ',')
        self.csv_quotation = self.config.get('settings', {}).get('csv_quotation', '"')

        if self.csv_delimiter.lower() == 'tab':
            self.csv_delimiter = '\t'

        try:
            self.echo_position = int(self.config.get('settings', {}).get('echo_position', 10000))
        except ValueError:
            self.echo_position = 10000

        if not hostname or not username or not password or not self.databases:
            print('Hostname, database, username, and password are all required in the [server] section of config.ini')
            sys.exit()

        self.delete_empty_files = self.config.get('settings', {}).get('delete_empty_files', False)

        if self.config.get('server', {}).get('version', 'MSSQL').upper() == 'MSSQL':
            self.connector = connectors.SQLServer()
        else:
            self.connector = connectors.MySQL()

        self.connector.connect(hostname=hostname, username=username, password=password, port=port)

        if run_immediately:
            self.process()

    def reset_vars(self):
        self.databases.clear()
        self.database_tables.clear()
        self.table_splits.clear()
        self.files.clear()

    def process(self):
        """ Process all database, tables..etc.

        :return:
        """
        for database in self.databases:
            database = database.strip()

            if not database:
                continue

            # Select the database
            self.connector.select_database(database)

            # Ensure the export folder for this database exists
            self.setup_database_folder(database)

            # Get all the tables we want to work on
            self.get_database_tables(database)

            print('\n########## {} ##########\n'.format(database))

            for table_name, query in self.database_tables.items():
                table_name = table_name.strip()

                if not table_name:
                    continue

                # Open the necessary csv files
                self.open_files(database, table_name)

                print(table_name, end=' ', flush=True)

                try:
                    # Obtain all table data
                    self.connector.cursor.execute(query)
                except Exception as e:
                    print(e)
                    print('Query Failed for {}, See message above.\n'.format(table_name))
                    continue

                # Obtain the columns for the table in question
                columns = self.get_columns()

                # Write the header row
                self.write_headers(columns)

                total_counter = 0

                while True:
                    row = self.connector.cursor.fetchone()

                    # Stop the while statement if fetchone returned None
                    if not row:
                        break

                    total_counter += 1

                    if total_counter % self.echo_position == 0:
                        print('{:,}'.format(total_counter), end=' ', flush=True)

                    row_data = self.connector.format_row_data(columns, row, self.encoding_read)
                    self.write_row_to_csv(database, table_name, total_counter, row_data)

                print('{:,} total records written over {:,} file(s).'.format(total_counter, len(self.files['open'])))

                self.close_files()

                if not total_counter:
                    self.empty_files.extend(self.files['paths'])

            self.remove_empty_files()

            print('Finished with {}!'.format(database))

        print('\nAll Jobs Complete!')

    def remove_empty_files(self):
        """ Removes files that had 0 rows written (aside from headers).

        :return:
        """
        if not self.delete_empty_files:
            return

        print('Deleting {} empty files'.format(len(self.empty_files)))

        for file_path in self.empty_files:
            try:
                os.remove(file_path)
            except (OSError, FileNotFoundError):
                pass

        self.empty_files.clear()

    def number_of_csv_files(self, database, table_name):
        """ Returns the number of CSV files we'll need for the supplied table_name, default is 1

        :param database:
        :param table_name:
        :return:
        """
        return self.table_splits.get(database, {}).get(table_name, 1)

    def write_row_to_csv(self, database, table_name, total_counter, row_data):
        """ Writes the rows data to the CSV file

        :param database:
        :param table_name:
        :param total_counter:
        :param row_data:
        :return:
        """
        try:
            num = 0
            number_of_splits = self.number_of_csv_files(database, table_name)

            if number_of_splits != 1:
                num = total_counter % number_of_splits

            self.files['csv'][num].writerow(row_data)

        except UnicodeEncodeError:
            print('\tEncode Error {} {} {}'.format(total_counter, database, table_name))
        except UnicodeDecodeError:
            print('\tDecode Error {} {} {}'.format(total_counter, database, table_name))
        except:
            print(total_counter, database, table_name)
            print(traceback.format_exc())

    def get_columns(self):
        """ Returns list containing all columns on the active sql query

        :return:
        """
        return [column[0] for column in self.connector.cursor.description]

    def write_headers(self, columns):
        """ Write all columns as a header row to all of the CSV files

        :param columns:
        :return:
        """
        [x.writerow(columns) for x in self.files['csv']]

    def get_database_tables(self, database):
        """ Obtain all user tables within the database

        No need for the databases name as a param since it would've been
            selected with the "USE..." statement.

        If the [queries] section in config.ini has entries, we will format
            them as needed and not bother query for tables.

        :return:
        """
        self.database_tables.clear()

        config_db_name = 'Database:{}'.format(database)
        split_values = self.config.get('Splits:{}'.format(database), {})

        # Since we store the query used in config.ini, use the table names and query given.
        for table_name, query in self.config.get(config_db_name, {}).items():

            self.database_tables[table_name] = query

            if database not in self.table_splits:
                self.table_splits[database] = {}

            self.table_splits[database][table_name] = split_values.get(table_name, 1)

        # If no tables were given, meaning we have an empty section, get all tables from the database itself.
        if not self.database_tables:

            for table_name in self.connector.get_database_tables(database):

                self.database_tables[table_name] = "SELECT * FROM {}".format(table_name)

        return self.database_tables

    def close_files(self):
        """ Close all files that we've opened

        :return:
        """
        [x.close() for x in self.files['open']]

    def open_files(self, database, table_name):
        """ Opens all files required for the table in question.

        :param database:
        :param table_name:
        :return:
        """
        self.files = {
            'csv': [],
            'open': [],
            'paths': [],
        }

        number_of_splits = self.number_of_csv_files(database, table_name)

        for i in range(number_of_splits):

            filename = '{}.csv'.format(table_name)
            if number_of_splits > 1:
                filename = '{}_{}.csv'.format(table_name, i)

            split_file_location = os.path.join(self.database_folder, filename)
            file_open = open(split_file_location, 'w', encoding=self.encoding_write, newline='')

            self.files['csv'].append(csv.writer(file_open, delimiter=self.csv_delimiter,
                                                quotechar=self.csv_quotation, quoting=csv.QUOTE_ALL))

            self.files['open'].append(file_open)
            self.files['paths'].append(split_file_location)

        return self.files

    def setup_database_folder(self, database):
        """ Ensure the databases file storage location exists

        :param database:
        :return:
        """
        self.database_folder = os.path.join(self.export_folder, database)
        if not os.path.isdir(self.database_folder):
            os.makedirs(self.database_folder)


if __name__ == '__main__':

    ddc = DumpDatabaseCSV()

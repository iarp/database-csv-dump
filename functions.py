import os
import sys
import datetime
import json

try:
    import configparser as cfgp
except ImportError:
    import ConfigParser as cfgp


def LoadConfig(file_location='config.ini', lower_all_keys=False):
    """
    Loads configuration ini files.
    :param lower_all_keys:
    :param file_location:
    """
    config = {}
    cp = cfgp.ConfigParser()
    cp.optionxform = str

    file_path = os.path.dirname(os.path.abspath(sys.argv[0]))
    file_location = os.path.join(file_path, file_location)
    cp.read(file_location)
    for original_section in cp.sections():

        sec = original_section
        if lower_all_keys:
            sec = sec.lower()

        if sec not in config:
            config[sec] = {}

        for original_option in cp.options(original_section):
            opt = original_option

            if lower_all_keys:
                opt = opt.lower()

            if opt not in config[sec]:
                config[sec][opt] = {}
            try:
                data = cp.get(original_section, original_option)
            except cfgp.InterpolationSyntaxError:
                data = 'Failed To Load, InterpolationSyntaxError! % must be escaped by another %'

            if data.lower() == 'true':
                data = True
            elif data.lower() == 'false':
                data = False

            if isinstance(data, str):
                try:
                    data = json.loads(data)
                except (ValueError, TypeError):
                    pass

            if isinstance(data, str):
                # Attempt to load a datetime stamp
                try:
                    data = datetime.datetime.strptime(data, '%Y-%m-%d %H:%M:%S')
                except ValueError:
                    pass

            config[sec][opt] = data

    return config


def SaveConfig(file_location, config):
    """
    Saves the configuration ini file.
    :type file_location: basestring
    :type config: dict
    """

    cp = cfgp.ConfigParser()
    cp.optionxform = str
    for folder_name in config:
        cp.add_section(folder_name)
        for field in config[folder_name]:
            field_value = config[folder_name][field]

            if isinstance(field_value, (dict, list)):
                try:
                    field_value = json.dumps(field_value)
                except ValueError:
                    pass
            elif isinstance(field_value, datetime.datetime):
                field_value = field_value.strftime('%Y-%m-%d %H:%M:%S')
            elif isinstance(field_value, int):
                field_value = str(field_value)
            elif field_value is True:
                field_value = 'True'
            elif field_value is False:
                field_value = 'False'

            cp.set(folder_name, field, field_value)

    file_path = os.path.dirname(os.path.abspath(sys.argv[0]))
    file_location = os.path.join(file_path, file_location)

    with open(file_location, 'w') as cfg:
        cp.write(cfg)

    return config
